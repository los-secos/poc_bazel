-> Command for build
bazel build //src/main/java/com/bmuschko:hello-world

-> Command for testing
bazel test //src/test/java/com/bmuschko/messenger:messenger-test-junit5
